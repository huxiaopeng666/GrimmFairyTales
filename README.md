# 🥔格林童话开发框架（GrimmFairyTales）
## 😀项目介绍
格林童话系列框架，本项目希望通过童话故事的表现形式，从零到一实现一个行业级的低代码、模块化的解决方案。

## 🔥项目章节（持续更新中...）
### 第一章 白雪公主 （snow-white）

<img src="https://img.apotato.cn/images/3b543921475dd90a1710505ff289eeee.jpeg" alt="3b543921475dd90a1710505ff289eeee" style="zoom:50%;" />

> 他就像是童话开篇所说，这是一个老掉牙的故事。讲述了白雪公主受到继母皇后的虐待，逃到森林里，遇到七个小矮人后，和王子重逢的故事。

同样白雪公主是整个故事的开篇，白雪的一般的童话，就像是项目将的开端，所以白雪篇将实现基础的项目架构和最基础的功能实现。

## 使用文档
- [**snow-white-common**](./snow-white/snow-white-common)
  - [snow-white-common-base](./snow-white/snow-white-common/snow-white-common-base/README.md)
  - [snow-white-common-base](./snow-white/snow-white-common/snow-white-common-cache/README.md)
  - [snow-white-common-core](./snow-white/snow-white-common/snow-white-common-core/README.md)
  - [snow-white-common-model](./snow-white/snow-white-common/snow-white-common-model/README.md)
- [**snow-white-modules**](./snow-white/snow-white-modules)
  - [snow-white-common-kafka](./snow-white/snow-white-modules/snow-white-modules-kafka-starter/README.md)
  - [snow-white-common-minio](./snow-white/snow-white-modules/snow-white-modules-minio-starter/README.md)
  - [snow-white-common-mybatisplus](./snow-white/snow-white-modules/snow-white-modules-mybatisplus-starter/README.md)
  - [snow-white-common-redis](./snow-white/snow-white-modules/snow-white-modules-redis-starter/README.md)
- [**apotato-test**](./snow-white/snow-white-test)
  - [apotato-modules-test](./snow-white/snow-white-test/snow-white-core-test/README.md)
