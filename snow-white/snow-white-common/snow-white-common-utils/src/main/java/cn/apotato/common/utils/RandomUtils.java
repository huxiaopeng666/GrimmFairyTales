package cn.apotato.common.utils;

import cn.hutool.core.util.RandomUtil;

import java.util.Random;

/**
 * 随机工具
 *
 * @author 胡晓鹏
 * @date 2023/05/23
 */
public class RandomUtils extends RandomUtil {

    /**
     * 随机手机号
     *
     * @return {@link String}
     */
    public static String randomPhoneNum() {
        String[] prefixes = {"133", "149", "153", "173", "177", "180", "181"};
        String prefix = prefixes[new Random().nextInt(prefixes.length)];
        return prefix + String.format("%08d", new Random().nextInt(99999999));
    }

}
