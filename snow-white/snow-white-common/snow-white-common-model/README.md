##

## Spring data jpa
### APIs 的详细文档:
1. Criteria API:这是Spring Data JPA定义的基于对象范型查询语言。它允许您创建类型安全的查询,并可与具体的数据库无关。
   文档:https://docs.spring.io/spring-data/jpa/docs/2.4.5/reference/html/#jpa.query-methods.criteria
2. @EntityGraph:这是用于定义实体图(Entity Graph)的注解。实体图允许在查询过程中指定需要包含的关联实体。
   文档:https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.entity-graph
3. FetchMode:这是用于控制关联实体的加载策略的枚举。它定义了 “急加载(Eager)”、“延迟加载(Lazy)”、“立即加载(Immediate)” 等不同的加载策略。
   文档:https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.entity-graph.fetch-graph
4. @OneToMany、@ManyToMany:这些是用于定义对象关系映射的注解。明确定义了实体之间的一对多、多对多关联关系。
   文档:@OneToMany - https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.one-to-many
   @ManyToMany - https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.many-to-many
   熟悉这些 APIs 及它们的用法,对于使用 Spring Data JPA 高效查询关联数据至关重要:
- Criteria API 用于创建类型安全查询。
- EntityGraph 用于指定查询时要包含的关联实体图。
- FetchMode 用于控制关联实体的加载策略。
- @OneToMany、@ManyToMany 注解用于定义实体之间的关联关系映射。