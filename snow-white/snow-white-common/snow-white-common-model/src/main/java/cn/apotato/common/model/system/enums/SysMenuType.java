package cn.apotato.common.model.system.enums;

/**
 * 系统菜单类型
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
public enum SysMenuType {
    // 1目录、2菜单、3按钮、
    CATALOGUE("目录", 1),
    MENU("菜单", 2),
    BUTTON("按钮", 3),
    ;
    public final String type;
    public final Integer value;

    SysMenuType(String type, Integer value) {
        this.type = type;
        this.value = value;
    }
}
