package cn.apotato.common.model.base;

import cn.apotato.common.core.group.ValidationGroups;
import cn.hutool.core.util.IdUtil;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * 分配id
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ColumnWidth(15)
@ContentRowHeight(23)
@ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER)
public class AssignIdBaseModel extends Model {

    @ExcelIgnore
    @TableId(type = IdType.ASSIGN_ID)
    @NotNull(message = "id不能为空", groups = {ValidationGroups.Update.class})
    private String id;

}
