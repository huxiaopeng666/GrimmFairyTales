package cn.apotato.common.model;

import cn.apotato.common.model.base.BaseModel;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 宠物
 *
 * @author 胡晓鹏
 * @date 2023/05/09
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("pet")
public class Pet extends BaseModel implements Serializable {

    /**
     * 名字
     */
    private String name;
    /**
     * 物种
     */
    private String species;

    private Long userId;

    @TableField(exist = false)
    private UserInfo user;

}
