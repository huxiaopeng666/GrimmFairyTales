package cn.apotato.common.model.system;

import cn.apotato.common.model.base.BaseModel;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import org.apache.ibatis.type.ArrayTypeHandler;

import java.util.List;

/**
 * 系统菜单权限
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
public class SysMenu extends BaseModel {
    /**
     * 名字
     */
    private String name;
    /**
     * url
     */
    private String url;
    /**
     * 许可
     */
    private String permission;

    /**
     * 类型: 1目录、2菜单、3按钮、
     */
    private Integer type;
    /**
     * 图标
     */
    private String icon;

    /**
     * 打开放式：单页single、新窗口new
     */
    private String openType;

    /**
     * 显示display、隐藏hide（按钮默认隐藏）
     */
    private String status;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * 请求方法
     */
    private String requestMethod;
    /**
     * 惟一id
     */
    private Long uniqueId;

    /**
     * 类型为2菜单 存在子菜单
     */
    @TableField(exist = false)
    private List<SysMenu> children;

    @ExcelIgnore
    private String orgId;

}
