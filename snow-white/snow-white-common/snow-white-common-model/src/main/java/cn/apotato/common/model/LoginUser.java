package cn.apotato.common.model;

import cn.apotato.common.model.system.SysUser;
import lombok.Data;

import java.util.Set;

/**
 * 登录用户
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
@Data
public class LoginUser {
    private static final long serialVersionUID = 1L;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 用户名id
     */
    private Long userid;
    /**
     * 组织id
     */
    private String orgId;
    /**
     * 账户
     */
    private String account;

    /**
     * 用户名
     */
    private String username;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 角色列表
     */
    private Set<String> roles;

    /**
     * 用户信息
     */
    private SysUser sysUser;
}
