package cn.apotato.common.model.base;

import cn.apotato.common.core.group.ValidationGroups;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 基本模型
 *
 * @author 胡晓鹏
 * @date 2023/04/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ColumnWidth(15)
@ContentRowHeight(23)
@ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER)
public class BaseModel extends Model implements Serializable {

    @ExcelIgnore
    @TableId(type = IdType.AUTO)
    @NotNull(message = "id不能为空", groups = {ValidationGroups.Update.class})
    private Long id;

}
