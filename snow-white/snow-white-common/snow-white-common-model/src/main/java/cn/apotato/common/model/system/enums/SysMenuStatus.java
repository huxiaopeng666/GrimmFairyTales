package cn.apotato.common.model.system.enums;

/**
 * 系统菜单类型
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
public enum SysMenuStatus {
    // 显示display、隐藏hide（按钮默认隐藏）
    DISPLAY("显示", "display"),
    HIDE("隐藏", "hide")
    ;
    public final String status;
    public final String value;

    SysMenuStatus(String status, String value) {
        this.status = status;
        this.value = value;
    }
}
