package cn.apotato.common.model.system;

import cn.apotato.common.model.base.AssignIdBaseModel;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统组织
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_organization")
public class SysOrganization extends AssignIdBaseModel {

    /**
     * 名字
     */
    private String name;

    /**
     * 组织代码
     */
    private String orgCode;

    /**
     * 上级组织代码
     */
    private String superiorOrganizationCode;

    /**
     * 组织类型
     */
    private Integer type;

    /**
     * 描述
     */
    private String description;

    @ExcelIgnore
    private String orgId;
}
