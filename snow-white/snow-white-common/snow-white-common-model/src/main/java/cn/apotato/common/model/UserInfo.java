package cn.apotato.common.model;

import cn.apotato.common.model.base.BaseModel;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 用户信息
 *
 * @author 胡晓鹏
 * @date 2023/05/09
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("user")
public class UserInfo extends BaseModel implements Serializable {

    /**
     * 名字
     */
    @ExcelProperty("名称")
    private String name;
    /**
     * 性别
     */
    @ExcelProperty("性别")
    private String sex;
    /**
     * 手机号
     */
    @ExcelProperty("手机号")
    private String phone;
    /**
     * 级别（大境界）：凡人、练气、筑基、金丹、元婴、化神、炼虚、合体、大乘、渡劫（三花聚顶）、真仙、金仙、太乙玉仙、大罗和道祖
     */
    @ExcelProperty("境界")
    private String level;

    /**
     * 等级（小境界）：1 - 10
     */
    @ExcelProperty("等级")
    private Integer grade;

    @ExcelIgnore
    @TableField(exist = false)
    private List<Pet> pets;

    @ExcelIgnore
    @TableField(exist = false)
    private Car car;

}
