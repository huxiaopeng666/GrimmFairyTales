package cn.apotato.common.model.system;

import cn.apotato.common.model.base.BaseModel;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统用户
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_user")
public class SysUser extends BaseModel {

    /**
     * 账户
     */
    private String account;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 电话
     */
    private String phone;
    /**
     * 状态 'normal','expired','locked'
     */
    private String status;

    @ExcelIgnore
    private String orgId;
}
