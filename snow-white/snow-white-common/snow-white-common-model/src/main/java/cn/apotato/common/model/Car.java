package cn.apotato.common.model;

import cn.apotato.common.model.base.BaseModel;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 车
 *
 * @author 胡晓鹏
 * @date 2023/05/09
 */
@TableName("car")
@EqualsAndHashCode(callSuper = true)
@Data
public class Car extends BaseModel implements Serializable {

    /**
     * 名字
     */
    private String name;

    /**
     * 价格
     */
    private Double price;

    /**
     * 用户id
     */
    private Long userId;

    @TableField(exist = false)
    private UserInfo user;

}
