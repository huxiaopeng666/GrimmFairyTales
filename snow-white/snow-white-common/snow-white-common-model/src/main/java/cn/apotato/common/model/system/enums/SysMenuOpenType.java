package cn.apotato.common.model.system.enums;

/**
 * 开放式系统菜单
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
public enum SysMenuOpenType {
    // 打开放式：单页single、新窗口new
    SINGLE("单页", "single"),
    NEW("新窗口", "new")
    ;
    public final String type;
    public final String value;

    SysMenuOpenType(String type, String value) {
        this.type = type;
        this.value = value;
    }
}
