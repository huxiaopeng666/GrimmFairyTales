package cn.apotato.common.model.base;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 胡晓鹏
 */
@Data
public class Model {
    /**
     * 创建时间
     */
    @JsonIgnore
    @ExcelIgnore
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdAt;

    /**
     * 修改时间
     */
    @JsonIgnore
    @ExcelIgnore
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;

    /**
     * 逻辑删除
     */
    @JsonIgnore
    @ExcelIgnore
    @TableLogic
    private Integer deleted;


    /**
     * 开始时间
     */
    @JsonIgnore
    @ExcelIgnore
    @TableField(exist = false)
    private LocalDateTime startedAt;

    /**
     * 结束时间
     */
    @JsonIgnore
    @ExcelIgnore
    @TableField(exist = false)
    private LocalDateTime endedAt;

    /**
     * 排序方式：倒叙=desc、正序=asc
     */
    @JsonIgnore
    @ExcelIgnore
    @TableField(exist = false)
    private String orderType;

    /**
     * 排序字段
     */
    @JsonIgnore
    @ExcelIgnore
    @TableField(exist = false)
    private List<String> orderFields;
}
