package cn.apotato.common.model.system;

import cn.apotato.common.model.base.BaseModel;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统作用
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sys_role")
public class SysRole extends BaseModel {
    /**
     * 名字
     */
    private String name;
    /**
     * 代码
     */
    private String code;
    /**
     * 状态: 正常normal 停用deactivate
     */
    private String status;

    @ExcelIgnore
    private String orgId;
}
