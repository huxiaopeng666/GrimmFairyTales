package cn.apotato.common.core.response;

import cn.apotato.common.core.enums.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 响应结果数据
 *
 * @author 胡晓鹏
 * @date 2023/05/23
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ResponseResultData<T> extends ResponseResult {

    private T data;

    public ResponseResultData(ResultCode resultCode, T t) {
        super(resultCode);
        this.data = t;
    }
    public ResponseResultData() {
    }

    /**
     * 成功
     *
     * @return {@link ResponseResult}
     */
    public ResponseResultData<T> success(String message, T result) {
        this.setSuccess(ResultCode.SUCCESS.getSuccess());
        this.setMessage(message);
        this.data = result;
        return this;
    }

    /**
     * 失败
     *
     * @return {@link ResponseResult}
     */
    public ResponseResultData<T> fail(String message) {
        this.setSuccess(ResultCode.CLIENT_FAIL.getSuccess());
        this.setMessage(message);
        return this;
    }

}
