package cn.apotato.common.core.exception;

/**
 * 远程访问异常
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
public class RemoteAccessException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public RemoteAccessException() {
        super("远程访问异常");
    }

    public RemoteAccessException(String message) {
        super(message);
    }


}
