package cn.apotato.common.core.constant;

/**
 * 安全常量
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
public class SecurityConstants
{
    /**
     * 用户ID字段
     */
    public static final String ROOT_USER = "admin";

    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";
    public static final String DETAILS_ORG_ID = "org_id";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";

    /**
     * 授权信息字段
     */
    public static final String AUTHORIZATION_HEADER = "authorization";

    /**
     * 请求来源
     */
    public static final String FROM_SOURCE = "from-source";

    /**
     * 内部请求
     */
    public static final String INNER = "inner";

    /**
     * 用户标识
     */
    public static final String USER_KEY = "user_key";

    /**
     * 登录用户
     */
    public static final String LOGIN_USER = "login_user";

    /**
     * 角色权限
     */
    public static final String ROLE_PERMISSION = "role_permission";

    public static final String PERMISSION_REDIS_KEY = "system.permissions.queue";
}

