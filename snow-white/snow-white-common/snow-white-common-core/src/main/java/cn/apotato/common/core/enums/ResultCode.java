package cn.apotato.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * 结果代码
 *
 * @author 胡晓鹏
 * @date 2023/05/23
 */
@Getter
@AllArgsConstructor
public enum ResultCode {

    /**
     * 成功
     */
    SUCCESS(true, HttpStatus.OK.value(), "请求成功"),
    /**
     * 服务器失败
     */
    SERVER_FAIL(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), "请求失败，服务端错误"),
    /**
     * url不存在
     */
    CLIENT_URL_FAIL(false, HttpStatus.BAD_REQUEST.value(), "请检查URL是否正确"),
    /**
     * 客户端失败
     */
    CLIENT_FAIL(false, HttpStatus.BAD_REQUEST.value(), "请求失败，客户端错误");

    private final Boolean success;

    private final Integer code;

    private final String message;

}
