package cn.apotato.common.core.exception;

/**
 * 内部身份验证异常
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
public class InnerAuthException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InnerAuthException(String message)
    {
        super(message);
    }
}
