package cn.apotato.common.core.context;

import cn.apotato.common.core.constant.SecurityConstants;
import cn.apotato.common.core.text.Convert;
import cn.apotato.common.core.utils.StringUtils;
import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 获取当前线程变量中的 用户id、用户名称、Token等信息
 * 注意： 必须在网关通过请求头的方法传入，同时在HeaderInterceptor拦截器设置值。 否则这里无法获取
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
public class SecurityContextHolder {
    private static final TransmittableThreadLocal<Map<String, Object>> THREAD_LOCAL = new TransmittableThreadLocal<>();

    public static void set(String key, Object value) {
        Map<String, Object> map = getLocalMap();
        map.put(key, value == null ? StringUtils.EMPTY : value);
    }

    public static String get(String key) {
        Map<String, Object> map = getLocalMap();
        return Convert.toStr(map.getOrDefault(key, StringUtils.EMPTY));
    }

    public static <T> T get(String key, Class<T> clazz) {
        Map<String, Object> map = getLocalMap();
        return StringUtils.cast(map.getOrDefault(key, null));
    }

    public static Map<String, Object> getLocalMap() {
        Map<String, Object> map = THREAD_LOCAL.get();
        if (map == null) {
            map = new ConcurrentHashMap<String, Object>();
            THREAD_LOCAL.set(map);
        }
        return map;
    }

    public static void setLocalMap(Map<String, Object> threadLocalMap) {
        THREAD_LOCAL.set(threadLocalMap);
    }

    /**
     * 得到用户id
     *
     * @return {@link Long}
     */
    public static Long getUserId() {
        return Convert.toLong(get(SecurityConstants.DETAILS_USER_ID), 0L);
    }

    /**
     * 得到组织id
     *
     * @return {@link String}
     */
    public static String getOrgId() {
        return get(SecurityConstants.DETAILS_ORG_ID);
    }

    /**
     * 设置组织id
     * @param orgId org id
     */
    public static void setOrgId(String orgId) {
        set(SecurityConstants.DETAILS_ORG_ID, orgId);
    }

    /**
     * 设置用户id
     *
     * @param account 账户
     */
    public static void setUserId(String account) {
        set(SecurityConstants.DETAILS_USER_ID, account);
    }

    /**
     * 获得用户名
     *
     * @return {@link String}
     */
    public static String getUserName() {
        return get(SecurityConstants.DETAILS_USERNAME);
    }

    /**
     * 设置用户名
     *
     * @param username 用户名
     */
    public static void setUserName(String username) {
        set(SecurityConstants.DETAILS_USERNAME, username);
    }

    /**
     * 获取用户关键
     *
     * @return {@link String}
     */
    public static String getUserKey() {
        return get(SecurityConstants.USER_KEY);
    }

    /**
     * 设置用户关键
     *
     * @param userKey 用户关键
     */
    public static void setUserKey(String userKey) {
        set(SecurityConstants.USER_KEY, userKey);
    }

    /**
     * 获得许可
     *
     * @return {@link String}
     */
    public static String getPermission() {
        return get(SecurityConstants.ROLE_PERMISSION);
    }

    /**
     * 设置权限
     *
     * @param permissions 权限
     */
    public static void setPermission(String permissions) {
        set(SecurityConstants.ROLE_PERMISSION, permissions);
    }

    public static void remove() {
        THREAD_LOCAL.remove();
    }
}
