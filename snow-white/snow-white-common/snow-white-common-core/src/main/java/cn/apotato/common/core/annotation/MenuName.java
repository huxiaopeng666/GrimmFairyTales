package cn.apotato.common.core.annotation;

import java.lang.annotation.*;

/**
 * 内部认证注解
 *
 * @author ruoyi
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MenuName {
    /**
     * 是否校验用户信息
     */
    String name() default "";
    String[] permission() default {};
}