package cn.apotato.common.core.constant;

/**
 * 服务名称常量
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
public class ServiceNameConstants {
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "snow-white-server-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String ADMIN_SERVICE = "snow-white-server-admin";
}
