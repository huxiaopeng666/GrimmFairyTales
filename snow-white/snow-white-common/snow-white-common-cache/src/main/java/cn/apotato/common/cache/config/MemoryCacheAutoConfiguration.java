package cn.apotato.common.cache.config;

import cn.apotato.common.cache.decorator.MemoryCache;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.cache.timeout.CacheTimeWheel;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 缓存自动配置
 *
 * @author 胡晓鹏
 * @date 2023/05/17
 */
@AutoConfigureOrder(Integer.MAX_VALUE)
@Configuration
@ConditionalOnMissingBean(name = "cache")
@EnableConfigurationProperties(CacheProperties.class)
public class MemoryCacheAutoConfiguration {
    private final CacheProperties cacheProperties;
    private final CacheTimeWheel cacheTimeWheel;

    public MemoryCacheAutoConfiguration(CacheProperties cacheProperties, CacheTimeWheel cacheTimeWheel) {
        this.cacheProperties = cacheProperties;
        this.cacheTimeWheel = cacheTimeWheel;
    }

    /**
     * redis缓存
     * @return {@link Cache}
     */
    @Bean(name = "cache")
    @ConditionalOnMissingBean(name = "cache")
    public Cache redisCache() {
        return new MemoryCache(cacheProperties, cacheTimeWheel);
    }

}
