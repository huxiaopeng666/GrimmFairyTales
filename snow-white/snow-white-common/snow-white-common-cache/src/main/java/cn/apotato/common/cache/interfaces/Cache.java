package cn.apotato.common.cache.interfaces;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 缓存
 *
 * @author 胡晓鹏
 * @date 2023/05/16
 */
public interface Cache {
    Set<String> KEYS = new CopyOnWriteArraySet<>();

    /**
     * 获取缓存数据
     *
     * @param key 关键
     * @return {@link Object}
     */
    Object get(String key);

    /**
     * 设置缓存数据
     *
     * @param key   关键
     * @param value 价值
     */
    void put(String key, Object value);

    /**
     * 删除缓存数据
     *
     * @param key 关键
     */
    void remove(String key);

    /**
     * 删除所有缓存数据
     */
    void removeAll();
}
