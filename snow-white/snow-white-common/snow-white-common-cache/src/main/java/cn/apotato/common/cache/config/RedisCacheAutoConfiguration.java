package cn.apotato.common.cache.config;

import cn.apotato.common.cache.decorator.RedisCache;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.modules.redis.service.RedisService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

/**
 * 缓存自动配置
 *
 * @author 胡晓鹏
 * @date 2023/05/17
 */
@Configuration
@EnableConfigurationProperties(CacheProperties.class)
@ConditionalOnClass(RedisConnectionFactory.class)
public class RedisCacheAutoConfiguration {
    private final CacheProperties cacheProperties;
    private final RedisService redisService;

    public RedisCacheAutoConfiguration(CacheProperties cacheProperties, RedisService redisService) {
        this.cacheProperties = cacheProperties;
        this.redisService = redisService;
    }

    /**
     * redis缓存
     * @return {@link Cache}
     */
    @Bean(name = "cache")
    @ConditionalOnMissingBean(name = "cache")
    public Cache redisCache() {
        return new RedisCache(redisService, cacheProperties);
    }
}
