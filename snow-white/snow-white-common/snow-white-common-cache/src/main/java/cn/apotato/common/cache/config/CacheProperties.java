package cn.apotato.common.cache.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 缓存属性
 *
 * @author 胡晓鹏
 * @date 2023/05/16
 */
@Data
@ConfigurationProperties(prefix = "cache")
public class CacheProperties {

    /**
     * 缓存时间 (单位：小时)
     */
    private Long cacheTimeOut = 3600000L;


}
