package cn.apotato.common.cache.timeout;

/**
 * 工作室功能
 *
 * @author 胡晓鹏
 * @date 2023/05/17
 */
@FunctionalInterface
public interface StudioFunction {

    /**
     * 调用
     *
     * @param timeStamp 时间戳
     */
    void invoke(Long timeStamp);
}
