package cn.apotato.common.cache.decorator;

import cn.apotato.common.cache.config.CacheProperties;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.cache.timeout.CacheTimeWheel;
import cn.apotato.common.cache.timeout.StudioFunction;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 内存缓存
 *
 * @author 胡晓鹏
 * @date 2023/05/16
 */
public class MemoryCache implements Cache {
    private final Map<String, Object> cache = new ConcurrentHashMap<>(128);
    private final CacheProperties cacheProperties;
    private final CacheTimeWheel cacheTimeWheel;

    public MemoryCache(CacheProperties cacheProperties, CacheTimeWheel cacheTimeWheel) {
        this.cacheProperties = cacheProperties;
        this.cacheTimeWheel = cacheTimeWheel;
    }

    /**
     * 获取缓存数据
     *
     * @param key 关键
     * @return {@link Object}
     */
    @Override
    public Object get(String key) {
        return cache.get(key);
    }

    /**
     * 设置缓存数据
     *
     * @param key   关键
     * @param value 价值
     */
    @Override
    public void put(String key, Object value) {
        cache.put(key, value);
        KEYS.add(key);
        cacheTimeWheel.setScheduledTasks(key, timeStamp -> {
            KEYS.remove(key);
        }, cacheProperties.getCacheTimeOut());
    }

    /**
     * 删除缓存数据
     *
     * @param key 关键
     */
    @Override
    public void remove(String key) {
        cache.remove(key);
        KEYS.remove(key);
        cacheTimeWheel.deleteScheduledTasks(key);
    }

    /**
     * 删除所有缓存数据
     */
    @Override
    public void removeAll() {
        cache.clear();
        KEYS.forEach(cacheTimeWheel::deleteScheduledTasks);
        KEYS.clear();
    }

}
