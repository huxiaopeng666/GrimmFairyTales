package cn.apotato.common.cache.timeout;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存时间轮
 *
 * @author 胡晓鹏
 * @date 2023/05/17
 */
@Slf4j
public class CacheTimeWheel {

    private final Map<Long, Map<String, StudioFunction>> timeDial = new ConcurrentHashMap<>();
    private final Map<String, Long> keyMap = new ConcurrentHashMap<>();

    private Boolean status = false;

    /**
     * 设定计划任务
     *
     * @param timeStamp      计划
     * @param studioFunction 工作室功能
     */
    public void setScheduledTasks(String key, StudioFunction studioFunction, Long timeStamp) {
        Long cacheTimeOut = +System.currentTimeMillis() + timeStamp;
        Map<String, StudioFunction> studioFunctionMap = timeDial.get(cacheTimeOut);
        if (studioFunctionMap == null) {
            studioFunctionMap = new ConcurrentHashMap<>(8);
        }
        studioFunctionMap.put(key, studioFunction);
        timeDial.put(cacheTimeOut, studioFunctionMap);
        if (!status) {
            rotateTheTimeWheel();
        }
        keyMap.put(key, cacheTimeOut);
    }

    /**
     * 被调度任务
     *
     * @param scheduled 计划
     * @return {@link Map}<{@link String}, {@link StudioFunction}>
     */
    public Map<String, StudioFunction> getScheduledTasks(Long scheduled) {
        return timeDial.get(scheduled);
    }

    /**
     * 被调度任务
     *
     * @param key 关键
     * @return {@link Map}<{@link String}, {@link StudioFunction}>
     */
    public StudioFunction getScheduledTasks(String key) {
        Long time = keyMap.get(key);
        Map<String, StudioFunction> studioFunctionMap = timeDial.get(time);
        if (studioFunctionMap == null) {
            return null;
        }
        return studioFunctionMap.get(key);
    }

    /**
     * 删除计划任务
     *
     * @param key 关键
     * @return {@link StudioFunction}
     */
    public StudioFunction deleteScheduledTasks(String key) {
        Long time = keyMap.get(key);
        Map<String, StudioFunction> studioFunctionMap = timeDial.get(time);
        if (studioFunctionMap == null) {
            return null;
        }
        return studioFunctionMap.remove(key);
    }

    /**
     * 旋转轮子
     */
    public void rotateTheTimeWheel() {
        status = true;
        CompletableFuture.runAsync(() -> {
            try {
                while (status) {
                    long timeMillis = System.currentTimeMillis();
                    Map<String, StudioFunction> studioFunctionMap = timeDial.get(timeMillis);
                    if (studioFunctionMap == null || studioFunctionMap.size() == 0) {
                        status = false;
                    }else {
                        studioFunctionMap.values().
                                forEach(studioFunction -> studioFunction.invoke(timeMillis));
                        timeDial.remove(timeMillis);
                        if (timeDial.isEmpty()) {
                            status = false;
                        }
                    }
                }
            } catch (Exception e) {
                status = false;
                log.error("Error while: {}", e.getMessage());
            }
        });
    }

}
