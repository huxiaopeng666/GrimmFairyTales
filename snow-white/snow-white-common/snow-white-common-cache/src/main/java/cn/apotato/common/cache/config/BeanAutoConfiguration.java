package cn.apotato.common.cache.config;

import cn.apotato.common.cache.timeout.CacheTimeWheel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * bean自动配置
 *
 * @author 胡晓鹏
 * @date 2023/05/17
 */
@Configuration
public class BeanAutoConfiguration {

    /**
     * 缓存时间轮
     *
     * @return {@link CacheTimeWheel}
     */
    @Bean
    public CacheTimeWheel cacheTimeWheel() {
        return new CacheTimeWheel();
    }

}
