package cn.apotato.common.cache.decorator;

import cn.apotato.common.cache.config.CacheProperties;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.modules.redis.service.RedisService;

import java.util.concurrent.TimeUnit;

/**
 * redis,缓存装饰
 *
 * @author 胡晓鹏
 * @date 2023/05/16
 */
public class RedisCache implements Cache {

    private final RedisService redisService;
    private final CacheProperties cacheProperties;

    public RedisCache(RedisService redisService, CacheProperties cacheProperties) {
        this.redisService = redisService;
        this.cacheProperties = cacheProperties;
    }


    /**
     * 获取缓存数据
     *
     * @param key 关键
     * @return {@link Object}
     */
    @Override
    public Object get(String key) {
        return redisService.getValue(key);
    }

    /**
     * 设置缓存数据
     *
     * @param key   关键
     * @param value 价值
     */
    @Override
    public void put(String key, Object value) {
        redisService.setValue(key, value, cacheProperties.getCacheTimeOut(), TimeUnit.HOURS);
        KEYS.add(key);
    }

    /**
     * 删除缓存数据
     *
     * @param key 关键
     */
    @Override
    public void remove(String key) {
        redisService.delete(key);
        KEYS.remove(key);
    }

    /**
     * 删除所有缓存数据
     */
    @Override
    public void removeAll() {
        redisService.deleteAll(KEYS);
        KEYS.clear();
    }
}
