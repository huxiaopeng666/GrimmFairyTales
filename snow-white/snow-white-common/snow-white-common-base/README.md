# snow-white-common-base
## 😀项目介绍
白雪公主系列-基础骨架: 使用mybatis-plus + mybatisplus-join 实现了CRUD的封装，实现了只需简单的继承几个类加上注解就可实现常见的增删改查的操作，支持多表查询等操作；简单的excel导出导入功能。

## 🔥功能（持续更新中...）
- 增删改查
- 多表关联查询
  - 一对一
  - 一对多
  - 自查询
- Excel表格导入导出
- 日志
- 等...
## 📚安装和使用
#### 安装

在maven中引入一下的依赖

```xml
<dependency>
    <groupId>cn.apotato</groupId>
    <artifactId>snow-white-common-base</artifactId>
    <version>${latest.version}</version>
</dependency>
```



#### 使用案例

使用 `snow-white-common-base` 实现CRUD和多变关联查询

##### 实体类

`账户表（account）实体类`

```java
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "account", autoResultMap = true)
public class Account extends BaseModel {

    /**
     * 账户
     */
    @NotBlank(message = "账号必填")
    private String account;

    /**
     * 密码
     */
    @NotBlank(message = "密码必填")
    private String password;

    /**
     * 电话
     */
    @NotEmpty(message = "电话必填")
    private String phone;

    /**
     * 用户名
     */
    private String username;

    /**
     * 组织id
     */
    @TableField(value = "org_id")
    @NotNull(message = "组织必填")
    private Long orgId;

    @TableField(exist = false)
    @EntityMapping(thisField = "orgId", joinField = "id")
    private Organization organization;

    /**
     * 汽车
     */
    @TableField(exist = false)
    @EntityMapping(thisField = "id", joinField = "userId")
    private List<Car> cars;

}
```



`组织表（organization）实体类`

```java
@Data
@Accessors(chain = true)
@TableName(value = "organization")
public class Organization extends BaseModel {

    /**
     * 名称
     */
    @NotEmpty(message = "组织名称必填")
    private String name;

    /**
     * 上级组织id
     */
    @NotNull(message = "上级组织必填")
    private Long pid;

    /**
     * 联系人
     */
    private String personLiable;

    /**
     * 联系方式
     */
    private String contactInformation;

}

```

`汽车表（car）实体类`

```java
@Data
@TableName("car")
@EqualsAndHashCode(callSuper = true)
public class Car extends BaseModel {

    /**
     * 名字
     */
    private String name;

    /**
     * 价格
     */
    private Double price;

    /**
     * 用户id
     */
    private Long userId;

    @TableField(exist = false)
    private UserInfo user;
}
```



##### mapper 层
`AccountMapper`

```java
@Mapper
public interface AccountMapper extends BaseMapper<Account> {
}
```

`OrganizationMapper`

```java
@Mapper
public interface OrganizationMapper extends BaseMapper<Organization> {
}
```
`CarMapper`

```java
@Mapper
public interface CarMapper extends BaseMapper<Car> {
}
```

##### **service层**

```java
public interface AccountService extends MPJDeepService<Account> {
}
```

```java
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {
    @Override
    public Class<Account> currentModelClass() {
        return Account.class;
    }
}
```

##### **controller层**

```java
@RequestMapping("account")
@RestController
public class AccountController extends BaseController<Account, Long> {

    public AccountController(MPJDeepService<Account> service, BaseMapper<Account> mapper, Cache cache) {
        super(service, mapper, cache);
    }

    // todo: 查询条件设置
    /**
     * 设置查询参数钩
     *
     * @param entity 实体
     * @return {@link QueryParamHookFunction}<{@link Account}>
     */
    @Override
    public QueryParamHookFunction<Account> setQueryParamHook(Account entity) {
        return parameter -> parameter;
    }

    /**
     * 设置查询包装钩子函数
     *
     * @param entity 实体
     * @return {@link QueryWrapperHookFunction}<{@link Account}>
     */
    @Override
    public QueryWrapperHookFunction<Account> setQueryWrapperHook(Account entity) {
        return lambdaQueryWrapper -> lambdaQueryWrapper;
    }

    // todo 三种不通颗粒度的查询结果过滤

    /**
     * 对查询结果操作的钩子
     *
     * @return {@link ResultPageFilterHookFunction}<{@link Account}>
     */
    @Override
    public ResultPageFilterHookFunction<Account> setResultFilterHook() {
        return page -> page;
    }

    /**
     * 对查询结果列表操作的钩子
     *
     * @return {@link ResultListFilterHookFunction}<{@link Account}>
     */
    @Override
    public ResultListFilterHookFunction<Account> setListFilterHookFunction() {
        return records -> records;
    }

    /**
     * 对查询结果中每个对象操作的钩子
     *
     * @return {@link ResultObjectFilterHook}<{@link Account}>
     */
    @Override
    public ResultObjectFilterHook<Account> setObjectFilterHook() {
        return object -> object;
    }
    
    
    
    /**
     * 级联查询 使用 JoinWrappers
     * ==> SELECT t.* o.NAME AS org_name FROM account t LEFT JOIN organization o ON o.id = t.org_id WHERE t.id = ?;
     *
     * @param accountId 帐户id
     * @return {@link List}<{@link AccountDTO}>
     */
    @GetMapping("join-lamda")
    public List<AccountDTO> getAccountInfoLamda(Long accountId) throws NoSuchMethodException, ClassNotFoundException {
        return JoinWrappers.lambda(Account.class)
                .selectAsClass(Account.class, AccountDTO.class)
                .selectAs(Organization::getName, AccountDTO::getOrgName)
                .leftJoin(Organization.class, Organization::getId, Account::getOrgId)
                .eq(accountId != null, "t.id", accountId)
                .list(AccountDTO.class);
    }
}
```



