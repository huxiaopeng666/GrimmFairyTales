package cn.apotato.common.base.processor;

import com.baomidou.mybatisplus.annotation.TableName;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;

/**
 * 基本mapper类处理器
 *
 * @author 胡晓鹏
 * @date 2023/05/18
 */
@SupportedAnnotationTypes("com.baomidou.mybatisplus.annotation.TableName")
public class BaseServiceImplClassProcessor extends AbstractProcessor {
    /**
     * {@inheritDoc}
     *
     * @param annotations
     * @param roundEnv
     */
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(TableName.class);
        // 为每个类生成BaseMapper
        for (Element element : elements) {
            TypeElement elementType = (TypeElement) element;
            //  获取当前类所在包名
            String packageName = processingEnv.getElementUtils().getPackageOf(elementType).getQualifiedName().toString();
            // 删除最后的包，添加mapper: com.google.entity -> com.google.mapper
            packageName = packageName.substring(0, packageName.lastIndexOf('.'));


            String entityName = elementType.getSimpleName().toString();
            String mapperName = entityName + "Mapper";
            String serviceName = entityName + "Service";
            String serviceImplName = entityName + "ServiceImpl";

            // 生成的Java代码
            String serviceImplCode =  "package "+packageName+".service.impl;\n" +
                    "\n" +
                    "import "+packageName+".entity."+entityName+";\n" +
                    "import "+packageName+".mapper."+mapperName+";\n" +
                    "import "+packageName+".service."+serviceName+";\n" +
                    "import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;\n" +
                    "import org.springframework.stereotype.Service;\n" +
                    "\n" +
                    "@Service\n" +
                    "public class "+serviceImplName+" extends ServiceImpl<"+mapperName+", "+entityName+"> implements "+serviceName+" {\n" +
                    "    @Override\n" +
                    "    public Class<"+entityName+"> currentModelClass() {\n" +
                    "        return "+entityName+".class;\n" +
                    "    }\n" +
                    "}\n";

            // 创建源文件,指定包名和类名
            JavaFileObject sourceFile2;
            try {
                sourceFile2 = processingEnv.getFiler().createSourceFile(packageName +"service.impl." + serviceImplName);
                // 写入源文件
                Writer writer = sourceFile2.openWriter();
                writer.write(serviceImplCode);
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return true;
    }
}
