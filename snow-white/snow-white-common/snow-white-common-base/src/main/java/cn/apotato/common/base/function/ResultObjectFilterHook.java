package cn.apotato.common.base.function;

/**
 * 结果对象过滤钩子
 *
 * @author 胡晓鹏
 * @date 2023/05/15
 */
@FunctionalInterface
public interface ResultObjectFilterHook<T> {
    /**
     * 钩
     *
     * @param object 对象
     * @return {@link T}
     */
    public T hook(T object);
}
