package cn.apotato.common.base.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 查询条件钩子函数
 *
 * @author 胡晓鹏
 * @date 2023/05/15
 */
@FunctionalInterface
public interface QueryWrapperHookFunction<T> {
    /**
     * 钩
     *
     * @param lambdaQueryWrapper λ查询包装
     * @return {@link LambdaQueryWrapper}<{@link T}>
     */
    LambdaQueryWrapper<T> hook(LambdaQueryWrapper<T> lambdaQueryWrapper);
}
