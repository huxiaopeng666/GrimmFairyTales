package cn.apotato.common.base.processor;

import com.baomidou.mybatisplus.annotation.TableName;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;

/**
 * 基本mapper类处理器
 *
 * @author 胡晓鹏
 * @date 2023/05/18
 */
@SupportedAnnotationTypes("com.baomidou.mybatisplus.annotation.TableName")
public class BaseMapperClassProcessor extends AbstractProcessor {
    /**
     * {@inheritDoc}
     *
     * @param annotations
     * @param roundEnv
     */
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(TableName.class);
        // 为每个类生成BaseMapper
        for (Element element : elements) {
            TypeElement elementType = (TypeElement) element;
            String className = elementType.getSimpleName() + "Mapper";
            //  获取当前类所在包名
            String packageName = processingEnv.getElementUtils().getPackageOf(elementType).getQualifiedName().toString();
            // 删除最后的包，添加mapper: com.google.entity -> com.google.mapper
            packageName = packageName.substring(0, packageName.lastIndexOf('.')) + "mapper";

            // 生成的Java代码
            String code = "package "+packageName+";\n" +
                    "\n" +
                    "import "+packageName + elementType.getSimpleName()+";\n" +
                    "import com.github.yulichang.base.MPJBaseMapper;\n" +
                    "import org.apache.ibatis.annotations.Mapper;\n" +
                    "\n" +
                    "@Mapper\n" +
                    "public interface " + className + " extends MPJBaseMapper<"+elementType.getSimpleName()+"> {\n" +
                    "}";
            // 创建源文件,指定包名和类名
            JavaFileObject sourceFile;
            try {
                sourceFile = processingEnv.getFiler().createSourceFile(packageName +"." + className);
                // 写入源文件
                Writer writer = sourceFile.openWriter();
                writer.write(code);
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return true;
    }
}
