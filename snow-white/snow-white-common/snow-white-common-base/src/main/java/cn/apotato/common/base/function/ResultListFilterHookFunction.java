package cn.apotato.common.base.function;

import java.util.List;

/**
 * 结果列表过滤钩子函数
 *
 * @author 胡晓鹏
 * @date 2023/05/15
 */
public interface ResultListFilterHookFunction<T> {
    /**
     * 钩
     *
     * @param records 记录
     * @return {@link List}<{@link T}>
     */
    List<T> hook(List<T> records);
}
