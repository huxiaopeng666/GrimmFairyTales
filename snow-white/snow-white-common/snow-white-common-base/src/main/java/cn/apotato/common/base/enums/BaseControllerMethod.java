package cn.apotato.common.base.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 基本控制器方法
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
public enum BaseControllerMethod {
    // CRUD methods
    CREATE("新增", "create", "POST", ""),
    FIND("详情", "findById", "GET", "/{id}"),
    PAGE("查询", "page", "GET", ""),
    UPDATE("修改", "update", "PUT", ""),
    DELETE("删除", "deleteById", "DELETE", "/{id}"),
    EXPORTING("导出", "exporting", "GET", "/export"),
    IMPORTING("导入", "importing", "POST", "/import"),
    ;
    public final String name;
    public final String value;
    public final String method;
    public final String url;

    BaseControllerMethod(String name, String value, String method, String url) {
        this.name = name;
        this.value = value;
        this.method = method;
        this.url = url;
    }

    public static BaseControllerMethod getMethod(String value) {
        for (BaseControllerMethod controllerMethod : BaseControllerMethod.values()) {
            if (controllerMethod.value.equals(value)) {
                return controllerMethod;
            }
        }
        return null;
    }

    /**
     * 方法列表
     *
     * @return {@link List}<{@link BaseControllerMethod}>
     */
    public static List<BaseControllerMethod> methodList() {
        return Arrays.stream(BaseControllerMethod.values()).collect(Collectors.toList());
    }
}
