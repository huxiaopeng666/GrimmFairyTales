package cn.apotato.common.base.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 基地映射器
 *
 * @author 胡晓鹏
 * @date 2023/05/11
 */
public interface BaseMapper<T> extends MPJBaseMapper<T> {

    /**
     * 选择
     *
     * @param tableName 表名
     * @param wrapper   包装器
     * @return {@link List}<{@link T}>
     */
    @Select("select * from ${tableName} ${ew.customSqlSegment}")
    List<T> select(@Param("tableName")String tableName, @Param(Constants.WRAPPER)Wrapper<T> wrapper);
}
