package cn.apotato.common.base.annotation;

import java.lang.annotation.*;

/**
 * 绑定实体
 *
 * @author 胡晓鹏
 * @date 2023/05/10
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface BindEntity {
    /**
     * 绑定的实体
     *
     * @return {@link Class}<{@link ?}>
     */
    Class<?> entity();

    /**
     * 被绑定实体id
     *
     * @return {@link String}
     */
    String entityId();

    /**
     * 绑定顶实体的id
     *
     * @return {@link String}
     */
    String bindId();

    /**
     * JOIN连接条件 优先级大于 entityId + bingId
     * 案例：user_address addr on addr.user_id = t.id
     * - t未当前表明的别名
     * @return {@link String}
     */
    String condition();

    /**
     * 深度绑定
     *
     * @return boolean
     */
    boolean deepBind() default false;
}
