package cn.apotato.common.base.annotation;

import cn.apotato.common.base.enums.DesensitizationMode;

import java.lang.annotation.*;

/**
 * 脱敏
 *
 * @author 胡晓鹏
 * @date 2023/05/23
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Desensitization {
    /**
     * 开始位置
     *
     * 脱敏开始位置 - 结束位置进行 "*" 的替换，默认全部字符全部替换为 “*”
     *
     *
     * @return Index of from
     */
    int from() default 0;

    /**
     * 结束位置
     *
     * 脱敏开始位置 - 结束位置进行 "*" 的替换，默认全部字符全部替换为 “*”
     *
     * @return Order of to
     */
    int to() default Integer.MAX_VALUE;

    /**
     * 字符
     *
     * @return {@link String}
     */
    String character() default "*";

    /**
     * 脱敏模式：
     * ALL: 查询和导出数据时都进行脱敏
     * SELECT: 查询时进行脱敏
     * EXPORT: 导出数据时进行脱敏
     *
     * @return {@link DesensitizationMode}
     */
    DesensitizationMode model() default DesensitizationMode.ALL;
}
