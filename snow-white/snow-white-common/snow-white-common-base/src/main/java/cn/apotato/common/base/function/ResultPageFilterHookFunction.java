package cn.apotato.common.base.function;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 查询结果过滤钩子函数
 *
 * @author 胡晓鹏
 * @date 2023/05/15
 */
@FunctionalInterface
public interface ResultPageFilterHookFunction<T> {

    /**
     * 查询结果过滤钩子
     *
     * @param page 页面
     * @return {@link IPage}<{@link T}>
     */
    Page<T> hook(Page<T> page);
}
