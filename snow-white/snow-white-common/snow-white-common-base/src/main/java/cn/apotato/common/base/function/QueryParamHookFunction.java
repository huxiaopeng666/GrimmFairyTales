package cn.apotato.common.base.function;

/**
 * 查询参数钩子函数
 *
 * @author 胡晓鹏
 * @date 2023/05/16
 */
@FunctionalInterface
public interface QueryParamHookFunction<T> {

    /**
     * 钩
     *
     * @param parameter 参数
     * @return {@link T}
     */
    T hook(T parameter);
}
