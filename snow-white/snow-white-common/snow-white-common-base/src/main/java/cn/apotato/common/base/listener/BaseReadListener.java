package cn.apotato.common.base.listener;

import cn.apotato.common.base.service.BaseService;
import cn.apotato.common.model.base.BaseModel;
import cn.apotato.common.model.base.Model;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 基础阅读侦听器
 *
 * @author 胡晓鹏
 * @date 2023/05/23
 */
@Slf4j
public class BaseReadListener <T extends Model, ID> implements ReadListener<T> {

    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;

    protected final BaseService<T, ID> service;

    /**
     * 缓存的数据
     */
    private List<T> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

    public BaseReadListener(BaseService<T, ID> service) {
        this.service = service;
    }

    /**
     * When analysis one row trigger invoke function.
     *
     * @param data    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context analysis context
     */
    @Override
    public void invoke(T data, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(data));
        cachedDataList.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (cachedDataList.size() >= BATCH_COUNT) {
            saveData();

        }
    }

    private void saveData() {
        service.saveBatch(cachedDataList);
        // 存储完成清理 list
        cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    }

    /**
     * if have something to do after all analysis
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        saveData();
    }
}
