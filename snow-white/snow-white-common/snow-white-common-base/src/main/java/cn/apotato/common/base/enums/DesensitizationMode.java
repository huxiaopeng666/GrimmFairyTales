package cn.apotato.common.base.enums;

/**
 * 脱敏模式
 *
 * @author 胡晓鹏
 * @date 2023/05/23
 */
public enum DesensitizationMode {
    /**
     * 所有
     */
    ALL(),
    /**
     * 查询
     */
    SELECT(),
    /**
     * 导出
     */
    EXPORT(),
    ;


    DesensitizationMode() {
    }
}
