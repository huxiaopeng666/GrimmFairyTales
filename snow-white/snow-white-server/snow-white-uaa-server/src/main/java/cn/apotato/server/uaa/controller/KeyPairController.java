package cn.apotato.server.uaa.controller;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;

/**
 * 密钥对控制器
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
@AllArgsConstructor
@RestController
public class KeyPairController {
    private final KeyPair keyPair;
    @GetMapping("/rsa/jwks.json")
    public Map<String, Object> getKey() {
        // 获取公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAKey key = new RSAKey.Builder(publicKey).build();
        return new JWKSet(key).toJSONObject();
    }
}
