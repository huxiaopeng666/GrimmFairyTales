package cn.apotato.server.uaa.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 密钥存储库属性
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
@Data
@ConfigurationProperties(prefix = "key-store")
public class KeyStoreProperties {

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 别名
     */
    private String alias;

    /**
     * 密码
     */
    private String password;
}
