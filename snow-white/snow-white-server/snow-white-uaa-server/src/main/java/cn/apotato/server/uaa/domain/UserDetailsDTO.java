package cn.apotato.server.uaa.domain;

import cn.apotato.server.uaa.enums.AccountStatus;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

/**
 * 用户详细信息dto
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
@Data
public class UserDetailsDTO implements UserDetails {

    private Long id;
    private String username;
    private String password;
    private Set<SimpleGrantedAuthority> authorities;

    /**
     * 账号状态
     */
    private String status;

    public UserDetailsDTO(Long id, String username, String password,String status, Set<SimpleGrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.status = status;
        this.authorities = authorities;
    }

    /**
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * @return
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * @return
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * 是账户非过期
     *
     * @return boolean
     */
    @Override
    public boolean isAccountNonExpired() {
        return AccountStatus.EXPIRED != AccountStatus.getStatus(this.status);
    }

    /**
     * 是账户非锁定
     *
     * @return boolean
     */
    @Override
    public boolean isAccountNonLocked() {
        return AccountStatus.LOCKED != AccountStatus.getStatus(this.status);
    }

    /**
     * 是凭证不过期
     *
     * @return boolean
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return AccountStatus.EXPIRED != AccountStatus.getStatus(this.status);
    }

    /**
     * 启用了
     * 状态正常就启用
     * @return boolean
     */
    @Override
    public boolean isEnabled() {
        return AccountStatus.NORMAL == AccountStatus.getStatus(this.status);
    }
}
