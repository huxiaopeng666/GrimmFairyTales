package cn.apotato.server.uaa.service;

import cn.apotato.common.model.system.SysMenu;
import cn.apotato.server.uaa.domain.UserDetailsDTO;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 用户详细信息服务impl
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    /**
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Long uerId = 1L;
        String password = "123456";
        List<SysMenu> sysMenuList = Arrays.asList(
                SysMenu.builder().url("system:user:view").build(),
                SysMenu.builder().url("system:user:add").build(),
                SysMenu.builder().url("system:user:list").build()
        );
        Set<SimpleGrantedAuthority> authorities = sysMenuList.stream()
                .map(SysMenu::getUrl)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
        return new UserDetailsDTO(uerId, username, password, "", authorities);
    }
}
