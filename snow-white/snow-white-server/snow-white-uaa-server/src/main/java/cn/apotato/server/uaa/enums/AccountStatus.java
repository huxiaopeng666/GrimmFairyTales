package cn.apotato.server.uaa.enums;

/**
 * 帐户状态
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
public enum AccountStatus {
    /**
     * 正常
     */
    NORMAL("normal"),
    /**
     * 过期
     */
    EXPIRED("expired"),
    /**
     * 已锁定
     */
    LOCKED("locked"),

    ;
    final String status;

    AccountStatus(String status) {
        this.status = status;
    }

    /**
     * 获得地位
     *
     * @param status 状态
     * @return {@link AccountStatus}
     */
    public static AccountStatus getStatus(String status) {
        for (AccountStatus accountStatus : AccountStatus.values()) {
            if (accountStatus.status.equals(status)) {
                return accountStatus;
            }
        }
        return NORMAL;
    }
}
