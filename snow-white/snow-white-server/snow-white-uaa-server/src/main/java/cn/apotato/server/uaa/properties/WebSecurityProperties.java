package cn.apotato.server.uaa.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 网络安全属性
 *
 * @author 胡晓鹏
 * @date 2023/05/26
 */
@Data
@ConfigurationProperties("web-security")
public class WebSecurityProperties {

    /**
     * 白名单
     */
    private List<String> whiteList;

    /**
     * 黑名单
     */
    private List<String> blackList;
}
