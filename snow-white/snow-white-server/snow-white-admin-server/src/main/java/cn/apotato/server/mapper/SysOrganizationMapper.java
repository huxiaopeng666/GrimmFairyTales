package cn.apotato.server.mapper;

import cn.apotato.common.model.system.SysOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sys组织映射器
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@Mapper
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {
}
