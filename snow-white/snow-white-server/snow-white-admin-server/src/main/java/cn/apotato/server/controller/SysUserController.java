package cn.apotato.server.controller;

import cn.apotato.common.base.controller.BaseController;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.model.system.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.extension.mapping.base.MPJDeepService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统用户控制器
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@RequestMapping("/system/user")
@RestController
public class SysUserController extends BaseController<SysUser, Long> {
    public SysUserController(MPJDeepService<SysUser> sysUserService, BaseMapper<SysUser> mapper, Cache cache) {
        super(sysUserService, mapper, cache);
    }

}
