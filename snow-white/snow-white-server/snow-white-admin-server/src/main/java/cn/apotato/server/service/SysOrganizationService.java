package cn.apotato.server.service;

import cn.apotato.common.model.system.SysOrganization;
import com.github.yulichang.extension.mapping.base.MPJDeepService;

/**
 * sys组织服务
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
public interface SysOrganizationService extends MPJDeepService<SysOrganization> {
}
