package cn.apotato.server.controller;

import cn.apotato.common.base.controller.BaseController;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.model.system.SysMenu;
import cn.apotato.common.model.system.SysRole;
import cn.apotato.server.service.SysMenuService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.extension.mapping.base.MPJDeepService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统用户控制器
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@RequestMapping("/system/menu")
@RestController
public class SysMenuController extends BaseController<SysMenu, Long> {

    private SysMenuService sysMenuService;

    public SysMenuController(SysMenuService sysMenuService, BaseMapper<SysMenu> mapper, Cache cache) {
        super(sysMenuService, mapper, cache);
        this.sysMenuService = sysMenuService;
    }

    /**
     * 创建
     *
     * @param entity 实体
     * @return {@link SysMenu}
     */
    @PostMapping
    @Override
    public SysMenu create(SysMenu entity) {
        return this.sysMenuService.create(entity);
    }




    /**
     * 批量存储
     *
     * @param sysMenuList 系统菜单列表
     */
    @PostMapping("batch")
    public void saveBatch(@RequestBody List<SysMenu> sysMenuList) {
        this.sysMenuService.saveBatchAndIgnore(sysMenuList);
    }

}
