package cn.apotato.server.service.impl;

import cn.apotato.common.model.system.SysRole;
import cn.apotato.server.mapper.SysRoleMapper;
import cn.apotato.server.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统角色服务impl
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
}
