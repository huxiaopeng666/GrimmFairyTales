package cn.apotato.server.service;

import cn.apotato.common.model.system.SysRole;
import com.github.yulichang.extension.mapping.base.MPJDeepService;

/**
 * 系统用户服务
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
public interface SysRoleService extends MPJDeepService<SysRole> {

}
