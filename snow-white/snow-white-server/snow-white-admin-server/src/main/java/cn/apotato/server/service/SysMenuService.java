package cn.apotato.server.service;

import cn.apotato.common.model.system.SysMenu;
import com.github.yulichang.extension.mapping.base.MPJDeepService;

import java.util.Collection;

/**
 * 系统用户服务
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
public interface SysMenuService extends MPJDeepService<SysMenu> {

    /**
     * 保存批处理和忽视
     *
     * @param menuList 菜单列表
     */
    void saveBatchAndIgnore(Collection<SysMenu> menuList);

    /**
     * 创建
     *
     * @param entity 实体
     * @return {@link SysMenu}
     */
    SysMenu create(SysMenu entity);
}
