package cn.apotato.server.handler;

import cn.apotato.common.model.LoginUser;
import cn.apotato.modules.mybatis.plus.functions.TenantIdFunction;
import cn.apotato.modules.mybatis.plus.plungs.TenantIdHandler;
import cn.apotato.security.utils.SecurityUtils;
import cn.apotato.server.service.SysOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 获取租者id处理程序
 *
 * @author 胡晓鹏
 * @date 2023/05/19
 */
@Component
public class MyTenantIdHandler implements TenantIdHandler {

    /**
     * 租户id
     *
     * @return {@link TenantIdFunction}
     */
    @Override
    public TenantIdFunction getTenantId() {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        String orgId;
        if (loginUser != null) {
            orgId = loginUser.getOrgId();
        } else {
            orgId = "snow-white-server-admin";
        }
        return () -> orgId;
    }

    /**
     * 获取租户id列
     *
     * @return {@link String}
     */
    @Override
    public String getTenantIdColumn() {
        return "org_id";
    }
}
