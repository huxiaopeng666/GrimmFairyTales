package cn.apotato.server.service;

import cn.apotato.common.model.system.SysUser;
import com.github.yulichang.extension.mapping.base.MPJDeepService;

/**
 * 系统用户服务
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
public interface SysUserService extends MPJDeepService<SysUser> {
}
