package cn.apotato.server.controller;

import cn.apotato.common.base.controller.BaseController;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.model.system.SysOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.extension.mapping.base.MPJDeepService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 组织系统控制器
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
@RequestMapping("/system/organization")
@RestController
public class SysOrganizationController extends BaseController<SysOrganization, String> {
    public SysOrganizationController(MPJDeepService<SysOrganization> iService, BaseMapper<SysOrganization> mapper, Cache cache) {
        super(iService, mapper, cache);
    }
}
