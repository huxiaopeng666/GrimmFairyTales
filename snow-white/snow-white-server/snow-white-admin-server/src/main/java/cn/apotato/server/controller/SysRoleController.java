package cn.apotato.server.controller;

import cn.apotato.common.base.controller.BaseController;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.model.system.SysRole;
import cn.apotato.common.model.system.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.extension.mapping.base.MPJDeepService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统用户控制器
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@RequestMapping("/system/role")
@RestController
public class SysRoleController extends BaseController<SysRole, Long> {

    public SysRoleController(MPJDeepService<SysRole> sysRoleService, BaseMapper<SysRole> mapper, Cache cache) {
        super(sysRoleService, mapper, cache);
    }
}
