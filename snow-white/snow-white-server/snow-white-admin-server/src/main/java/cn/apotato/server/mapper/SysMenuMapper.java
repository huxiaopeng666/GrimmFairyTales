package cn.apotato.server.mapper;

import cn.apotato.common.model.system.SysMenu;
import cn.apotato.common.model.system.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Collection;

/**
 * 系统用户映射器
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {
}
