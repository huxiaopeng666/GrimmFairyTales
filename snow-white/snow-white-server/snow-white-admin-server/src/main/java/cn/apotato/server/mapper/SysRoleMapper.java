package cn.apotato.server.mapper;

import cn.apotato.common.model.system.SysRole;
import cn.apotato.common.model.system.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户映射器
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
