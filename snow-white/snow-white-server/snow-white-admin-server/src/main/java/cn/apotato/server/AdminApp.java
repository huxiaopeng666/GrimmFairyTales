package cn.apotato.server;

import cn.apotato.server.annotation.EnableSnowWhite;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 管理应用程序
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@EnableSnowWhite
@SpringBootApplication
public class AdminApp {
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    public static void main(String[] args) {
        SpringApplication.run(AdminApp.class, args);
    }
}
