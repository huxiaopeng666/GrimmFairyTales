package cn.apotato.server.service.impl;

import cn.apotato.common.model.system.SysUser;
import cn.apotato.server.mapper.SysUserMapper;
import cn.apotato.server.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统用户服务impl
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
}
