package cn.apotato.server.service.impl;

import cn.apotato.common.model.system.SysOrganization;
import cn.apotato.server.mapper.SysOrganizationMapper;
import cn.apotato.server.service.SysOrganizationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * sys impl组织服务
 *
 * @author 胡晓鹏
 * @date 2023/05/29
 */
@Service
public class SysOrganizationServiceImpl extends ServiceImpl<SysOrganizationMapper, SysOrganization> implements SysOrganizationService {
}
