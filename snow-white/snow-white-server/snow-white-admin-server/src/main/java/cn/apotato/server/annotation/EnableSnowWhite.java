package cn.apotato.server.annotation;

import cn.apotato.security.annotation.EnableCustomConfig;
import cn.apotato.security.annotation.EnableRyFeignClients;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.lang.annotation.*;


/**
 * 让白雪公主
 *
 * @author 胡晓鹏
 * @date 2023/06/05
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@EnableTransactionManagement
@EnableRyFeignClients
@EnableCustomConfig
@EnableDiscoveryClient
public @interface EnableSnowWhite {
}
