package cn.apotato.server.runner;

import cn.apotato.security.handler.CollectPermissionsHandler;
import cn.apotato.security.runner.PermissionRunner;
import cn.apotato.server.consumer.PermissionConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

/**
 * spring应用程序跑
 *
 * @author 胡晓鹏
 * @date 2023/06/02
 */
@Slf4j
@Component
public class SpringAppRunner extends PermissionRunner {

    private final PermissionConsumer permissionConsumer;
    public SpringAppRunner(CollectPermissionsHandler collectPermissionsHandler, PermissionConsumer permissionConsumer) {
        super(collectPermissionsHandler);
        this.permissionConsumer = permissionConsumer;
    }

    /**
     * 运行
     *
     * @param args arg游戏
     * @throws Exception 异常
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 收集权限
        super.collectPermissionsHandler.collect();
        // 权限更新/录入
        permissionConsumer.run();
    }
}
