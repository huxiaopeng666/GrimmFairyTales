package cn.apotato.modules.test.mapper;

import cn.apotato.common.model.Car;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 汽车映射器
 *
 * @author 胡晓鹏
 * @date 2023/05/19
 */
@Mapper
public interface CarMapper extends BaseMapper<Car> {
}
