package cn.apotato.modules.test.entity;

import cn.apotato.common.base.annotation.Desensitization;
import cn.apotato.common.base.enums.DesensitizationMode;
import cn.apotato.common.model.base.BaseModel;
import cn.apotato.common.model.Car;
import cn.apotato.modules.mybatis.plus.handler.TowArrayTypeHandler;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.github.yulichang.annotation.EntityMapping;
import lombok.*;
import org.apache.ibatis.type.ArrayTypeHandler;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * 账户
 * 账户的角色和菜单是分离的设计，角色的分配又前段控制。
 * 账户本身就是一个角色，而角色作为业务需求使用
 *
 * @author xphu
 * @date 2022/11/25
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName(value = "account", autoResultMap = true)
public class Account extends BaseModel {

    /**
     * 账户
     */
    @ExcelProperty("账号")
    @NotBlank(message = "账号必填")
    private String account;

    /**
     * 密码
     */
    @Desensitization
    @ExcelProperty("密码")
    @NotBlank(message = "密码必填")
    private String password;

    /**
     * 电话
     */
    @Desensitization(from = 3, to = 8, model = DesensitizationMode.SELECT)
    @ExcelProperty("电话")
    @NotEmpty(message = "电话必填")
    private String phone;

    /**
     * 用户名
     */
    @ExcelProperty("用户名")
    private String username;

    /**
     * 邮件
     */
    @ExcelProperty("邮件")
    private String mail;
    /**
     * 地址
     */
    @ExcelProperty("地址")
    private String address;

    /**
     * 昵称
     */
    @ExcelProperty("昵称")
    private String nickname;

    /**
     * 微信openId
     */
    private String openId;
    /**
     * 小程序使用
     */
    private String sessionKey;

    /**
     * 微信公众号
     */
    private String unionId;

    /**
     * 微信公众号
     */
    private String publicOpenId;

    /**
     * 是否关注公众号
     */
    private Boolean subscribeFlag;

    @ExcelIgnore
    @TableField(exist = false)
    @EntityMapping(thisField = "orgId", joinField = "id")
    private Organization organization;

    @ExcelIgnore
    @TableField(exist = false)
    @EntityMapping(thisField = "id", joinField = "userId")
    private List<Car> cars;

    /**
     * 角色
     */
    @ExcelIgnore
    @TableField(typeHandler = ArrayTypeHandler.class)
    private Long[] roleId;

    /**
     * 菜单
     */
    @ExcelIgnore
    @TableField(typeHandler = ArrayTypeHandler.class)
    private String[] menuId;


    /**
     * 矩阵 二维数组
     */
    @ExcelIgnore
    @TableField(value = "matrix",typeHandler = TowArrayTypeHandler.class)
    private Integer[][] matrix;

    @ExcelIgnore
    @TableField(exist = false)
    private Long rootOrgId;

    private Long orgId;

}
