package cn.apotato.modules.test.entity;

import cn.apotato.common.model.base.BaseModel;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.ArrayTypeHandler;

/**
 * 角色
 *
 * @author 胡晓鹏
 * @date 2023/05/23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "role",autoResultMap = true)
public class Role extends BaseModel {

    /**
     * 角色名字
     */
    private String name;
    /**
     * 类型：0系统预设、1自定义创建
     */
    private Integer type;

    /**
     * 菜单
     */
    @TableField(typeHandler = ArrayTypeHandler.class)
    private String[] menuId;
}
