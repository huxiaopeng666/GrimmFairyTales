package cn.apotato.modules.test.controller;

import cn.apotato.common.base.controller.BaseController;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.core.annotation.MenuName;
import cn.apotato.modules.test.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.extension.mapping.base.MPJDeepService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 缓存控制器
 *
 * @author 胡晓鹏
 * @date 2023/05/18
 */
@MenuName(name = "缓存管理", permission = "test:cache")
@RequestMapping("/cache")
@RestController
public class CacheController extends BaseController<Account, Long> {
    private final Cache cache;

    public CacheController(MPJDeepService<Account> iService, BaseMapper<Account> mapper, Cache cache) {
        super(iService, mapper, cache);
        this.cache = cache;
    }

    @GetMapping("/set")
    public String setCache(String key, String value) {
        cache.put(key, value);
        return (String) cache.get(key);
    }

    @GetMapping("/get")
    public String getCache(String key) {
        Object o = cache.get(key);
        return o == null ? "" : o.toString();
    }

    @GetMapping("/delete")
    public void deleteCache(String key) {
        cache.remove(key);
    }

}
