package cn.apotato.modules.test.service;

import cn.apotato.modules.test.entity.Account;
import com.github.yulichang.extension.mapping.base.MPJDeepService;

/**
 * 帐户服务
 *
 * @author 胡晓鹏
 * @date 2023/04/21
 */
public interface AccountService extends MPJDeepService<Account> {
}
