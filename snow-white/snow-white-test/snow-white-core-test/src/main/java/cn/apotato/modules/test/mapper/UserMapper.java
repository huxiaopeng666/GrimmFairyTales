package cn.apotato.modules.test.mapper;

import cn.apotato.common.model.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户映射器
 *
 * @author 胡晓鹏
 * @date 2023/05/18
 */
@Mapper
public interface UserMapper  extends BaseMapper<UserInfo> {
}
