package cn.apotato.modules.test.mapper;

import cn.apotato.modules.test.entity.Organization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 组织映射器
 *
 * @author 胡晓鹏
 * @date 2023/05/05
 */
@Mapper
public interface OrganizationMapper extends BaseMapper<Organization> {
}
