package cn.apotato.modules.test;

import cn.apotato.security.annotation.EnableCustomConfig;
import cn.apotato.security.annotation.EnableRyFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 测试程序运行
 *
 * @author 胡晓鹏
 * @date 2023/03/02
 */
@EnableCaching
@EnableRyFeignClients
@EnableCustomConfig
@EnableDiscoveryClient
@SpringBootApplication
public class TestAppRun {
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    public static void main(String[] args) {
        SpringApplication.run(TestAppRun.class);
    }
}
