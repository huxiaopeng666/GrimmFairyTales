package cn.apotato.modules.test.controller;

import cn.apotato.common.base.controller.BaseController;
import cn.apotato.common.base.function.*;
import cn.apotato.common.cache.interfaces.Cache;
import cn.apotato.common.core.annotation.MenuName;
import cn.apotato.modules.test.entity.Account;
import cn.apotato.modules.test.entity.Organization;
import cn.apotato.modules.test.pojo.AccountDTO;
import cn.apotato.security.annotation.RequiresRoles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.extension.mapping.base.MPJDeepService;
import com.github.yulichang.toolkit.JoinWrappers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * BaseController和MPJBaseMapper测试
 *
 * @author 胡晓鹏
 * @date 2023/04/21
 */
@MenuName(name = "账户管理", permission = "test:account")
@RequestMapping(value = "account")
@RestController
public class AccountController extends BaseController<Account, Long> {
    public AccountController(MPJDeepService<Account> iService, BaseMapper<Account> mapper, Cache cache) {
        super(iService, mapper, cache);
    }

    /**
     * 级联查询 MPJLambdaWrapper的简单使用
     * <a href="https://mybatisplusjoin.com/pages/core/str/select.html">MPJLambdaWrapper文档</a>
     * ==> SELECT t.* o.NAME AS org_name FROM account t LEFT JOIN organization o ON o.id = t.org_id WHERE t.id = ?;
     *
     * @param accountId 帐户id
     * @return {@link List}<{@link AccountDTO}>
     */
    @RequiresRoles(value = {"admin", "root"}, name = "级联查询")
    @GetMapping("join-lamda")
    public List<AccountDTO> getAccountInfoLamda(Long accountId) {
        return JoinWrappers.lambda(Account.class)
                .selectAsClass(Account.class, AccountDTO.class)
                .selectAs(Organization::getName, AccountDTO::getOrgName)
                .eq(accountId != null, "t.id", accountId)
                .list(AccountDTO.class);
    }

    // todo: 查询条件设置
    /**
     * 设置查询参数钩
     *
     * @param entity 实体
     * @return {@link QueryParamHookFunction}<{@link Account}>
     */
    @Override
    public QueryParamHookFunction<Account> setQueryParamHook(Account entity) {
        return parameter -> parameter;
    }

    /**
     * 设置查询包装钩子函数
     *
     * @param entity 实体
     * @return {@link QueryWrapperHookFunction}<{@link Account}>
     */
    @Override
    public QueryWrapperHookFunction<Account> setQueryWrapperHook(Account entity) {
        return lambdaQueryWrapper -> lambdaQueryWrapper;
    }

    // todo 三种不通颗粒度的查询结果过滤

    /**
     * 设置结果过滤钩子
     *
     * @return {@link ResultPageFilterHookFunction}<{@link Account}>
     */
    @Override
    public ResultPageFilterHookFunction<Account> setResultFilterHook() {
        return page -> page;
    }

    /**
     * 对查询结果列表操作的钩子
     *
     * @return {@link ResultListFilterHookFunction}<{@link Account}>
     */
    @Override
    public ResultListFilterHookFunction<Account> setListFilterHookFunction() {
        return records -> records;
    }

    /**
     * 对查询结果中每个对象操作的钩子
     *
     * @return {@link ResultObjectFilterHook}<{@link Account}>
     */
    @Override
    public ResultObjectFilterHook<Account> setObjectFilterHook() {
        return object -> object;
    }
}
