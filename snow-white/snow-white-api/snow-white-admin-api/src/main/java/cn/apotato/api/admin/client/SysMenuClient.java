package cn.apotato.api.admin.client;

import cn.apotato.api.admin.factory.SysMenuFallbackFactory;
import cn.apotato.common.core.response.ResponseResultData;
import cn.apotato.common.model.system.SysMenu;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cn.apotato.common.core.constant.ServiceNameConstants.ADMIN_SERVICE;

/**
 * 系统菜单客户
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
@FeignClient(contextId = "sysMenuClient", value = ADMIN_SERVICE, fallbackFactory = SysMenuFallbackFactory.class)
public interface SysMenuClient {
    /**
     * 页面
     *
     * @param sysMenu 系统菜单
     * @param current 当前
     * @param size    大小
     * @return {@link ResponseResultData}<{@link Page}<{@link SysMenu}>>
     * @throws IllegalAccessException 非法访问异常
     */
    @GetMapping("/system/menu")
    ResponseResultData<Page<SysMenu>> page(@RequestParam("current") Long current, @RequestParam("size") Long size, @ModelAttribute SysMenu sysMenu) throws IllegalAccessException;

    /**
     * 创建
     *
     * @param sysMenu 系统菜单
     * @return {@link SysMenu}
     */
    @PostMapping("/system/menu")
    ResponseResultData<SysMenu> create(@RequestBody SysMenu sysMenu);

    /**
     * 保存批
     *
     * @param sysMenuList 系统菜单列表
     * @return {@link ResponseResultData}
     */
    @PostMapping("/system/menu/batch")
    ResponseResultData<Object> saveBatch(@RequestBody List<SysMenu> sysMenuList);
}
