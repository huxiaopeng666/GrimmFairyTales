package cn.apotato.api.admin.client;

import cn.apotato.api.admin.factory.SysUserFallbackFactory;
import cn.apotato.common.core.response.ResponseResultData;
import cn.apotato.common.model.system.SysUser;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static cn.apotato.common.core.constant.ServiceNameConstants.ADMIN_SERVICE;

/**
 * 系统用户客户端
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
@FeignClient(contextId = "sysUserClient", value = ADMIN_SERVICE, fallbackFactory = SysUserFallbackFactory.class)
public interface SysUserClient {
    /**
     * 页面
     *
     * @param page    页面
     * @param sysUser 系统用户
     * @return {@link Page}<{@link SysUser}>
     * @throws IllegalAccessException 非法访问异常
     */
    @GetMapping("/system/user")
    ResponseResultData<Page<SysUser>> page(Page<SysUser> page, SysUser sysUser) throws IllegalAccessException;

    /**
     * 创建
     *
     * @param sysUser 系统用户
     * @return {@link SysUser}
     */
    @PostMapping("/system/user")
    ResponseResultData<SysUser> create(@RequestBody SysUser sysUser);
}
