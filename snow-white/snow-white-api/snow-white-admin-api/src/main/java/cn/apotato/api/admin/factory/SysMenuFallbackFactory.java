package cn.apotato.api.admin.factory;

import cn.apotato.api.admin.client.SysMenuClient;
import cn.apotato.common.core.response.ResponseResultData;
import cn.apotato.common.model.system.SysMenu;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 系统用户后备工厂
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
@Component
public class SysMenuFallbackFactory implements FallbackFactory<SysMenuClient> {
    /**
     * Returns an instance of the fallback appropriate for the given cause.
     *
     * @param cause cause of an exception.
     * @return fallback
     */
    @Override
    public SysMenuClient create(Throwable cause) {
        return new SysMenuClient() {


            /**
             * 页面
             *
             * @param sysMenu 系统菜单
             * @param current 当前
             * @param size    大小
             * @return {@link ResponseResultData}<{@link Page}<{@link SysMenu}>>
             */
            @Override
            public ResponseResultData<Page<SysMenu>> page(@RequestParam("current") Long current, @RequestParam("size") Long size, SysMenu sysMenu) {
                return new ResponseResultData<Page<SysMenu>>().fail("查询失败");
            }

            /**
             * 创建
             *
             * @param sysMenu 系统菜单
             * @return {@link SysMenu}
             */
            @Override
            public ResponseResultData<SysMenu> create(SysMenu sysMenu) {
                return new ResponseResultData<SysMenu>().fail("创建失败");
            }

            /**
             * 保存批
             *
             * @param sysMenuList 系统菜单列表
             * @return {@link ResponseResultData}
             */
            @Override
            public ResponseResultData<Object> saveBatch(List<SysMenu> sysMenuList) {
                return new ResponseResultData<>().fail("创建失败");
            }
        };
    }
}
