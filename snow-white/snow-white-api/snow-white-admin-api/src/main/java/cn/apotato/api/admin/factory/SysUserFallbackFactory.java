package cn.apotato.api.admin.factory;

import cn.apotato.api.admin.client.SysUserClient;
import cn.apotato.common.core.response.ResponseResultData;
import cn.apotato.common.model.system.SysUser;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 系统用户后备工厂
 *
 * @author 胡晓鹏
 * @date 2023/05/31
 */
@Component
public class SysUserFallbackFactory implements FallbackFactory<SysUserClient> {
    /**
     * Returns an instance of the fallback appropriate for the given cause.
     *
     * @param cause cause of an exception.
     * @return fallback
     */
    @Override
    public SysUserClient create(Throwable cause) {
        return new SysUserClient() {

            /**
             * 页面
             *
             * @param page    页面
             * @param sysUser 系统用户
             * @return {@link Page}<{@link SysUser}>
             */
            @Override
            public ResponseResultData<Page<SysUser>>page(Page<SysUser> page, SysUser sysUser) {
                return new ResponseResultData<Page<SysUser>>().fail("查询失败");
            }

            /**
             * 创建
             *
             * @param sysUser 系统用户
             * @return {@link SysUser}
             */
            @Override
            public ResponseResultData<SysUser> create(SysUser sysUser) {
                return new ResponseResultData<SysUser>().fail("创建用户失败");
            }
        };
    }
}
