package cn.apotato.security.annotation;

import cn.apotato.security.config.AppConfig;
import cn.apotato.security.feign.FeignAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

import java.lang.annotation.*;

/**
 * 启用自定义配置
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
// 指定要扫描的Mapper类的包的路径
// 开启线程异步执行
@EnableAsync
// 自动加载类
@Import({AppConfig.class, FeignAutoConfiguration.class})
public @interface EnableCustomConfig {

}
