package cn.apotato.security.runner;

import cn.apotato.security.handler.CollectPermissionsHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 允许收集器
 *
 * @author 胡晓鹏
 * @date 2023/05/30
 */
@Slf4j
@Component
public class PermissionRunner implements ApplicationRunner {

    protected final CollectPermissionsHandler collectPermissionsHandler;

    public PermissionRunner(CollectPermissionsHandler collectPermissionsHandler) {
        this.collectPermissionsHandler = collectPermissionsHandler;
    }

    /**
     * 运行
     *
     * @param args arg游戏
     * @throws Exception 异常
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 收集权限
        collectPermissionsHandler.collect();
    }


}
