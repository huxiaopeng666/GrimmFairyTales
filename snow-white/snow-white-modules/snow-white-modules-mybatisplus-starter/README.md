# Mybatis-Plus-Snow-White

> snow-white-modules-mybatisplus-starter

## 😀项目介绍

白雪公主系列-ORM框架：在Mybatis-Plus基础上又做了一些增强和简化，使一些配置可以更好的进行配置

## 🔥功能（持续更新中...）

- 增加分页插件的配置
- 增加多租户插件的配置
- 增加默认的自动填充：createdAt、updatedAt
- 增加序列化枚举值的处理
- 

## 📚安装和使用

#### 安装

在maven中引入一下的依赖

```xml
<dependency>
    <groupId>cn.apotato</groupId>
    <artifactId>snow-white-common-cache</artifactId>
    <version>${latest.version}</version>
</dependency>
```



#### 使用案例

使用 `snow-white-common-cache` 

```java
public class CacheTest {
    private final Cache cache;

    public String setCache(String key, String value) {
        cache.put(key, value);
        return (String) cache.get(key);
    }

    public String getCache(String key) {
        Object o = cache.get(key);
        return o == null ? "" : o.toString();
    }

    public void deleteCache(String key) {
        cache.remove(key);
    }

}
```

