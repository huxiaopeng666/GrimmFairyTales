package cn.apotato.modules.mybatis.plus.handler;

/**
 * 整两个数组处理程序
 * inteager两个数组处理程序
 *
 * @author 胡晓鹏
 * @date 2023/05/22
 */
public class IntegerBaseTwoArrayHandler extends BaseTwoArrayHandler<Integer> {
    /**
     * 得到类型
     *
     * @return {@link Class}<{@link Integer}>
     */
    @Override
    protected Class<Integer> getType() {
        return Integer.class;
    }
}
