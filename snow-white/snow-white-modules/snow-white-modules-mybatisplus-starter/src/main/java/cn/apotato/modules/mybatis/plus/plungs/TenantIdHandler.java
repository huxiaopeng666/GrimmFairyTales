package cn.apotato.modules.mybatis.plus.plungs;

import cn.apotato.modules.mybatis.plus.functions.TenantIdFunction;

/**
 * 租户处理程序
 *
 * @author 胡晓鹏
 * @date 2023/05/19
 */
public interface TenantIdHandler {

    /**
     * 租户id
     *
     * @return {@link TenantIdFunction}
     */
    public TenantIdFunction getTenantId();

    /**
     * 获取租户id列
     *
     * @return {@link String}
     */
    default String getTenantIdColumn() {
        return "org_id";
    }
}
