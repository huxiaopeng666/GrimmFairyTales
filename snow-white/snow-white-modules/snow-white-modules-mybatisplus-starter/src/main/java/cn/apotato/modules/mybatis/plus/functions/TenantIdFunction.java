package cn.apotato.modules.mybatis.plus.functions;

/**
 * 承租者id函数
 *
 * @author 胡晓鹏
 * @date 2023/05/19
 */
@FunctionalInterface
public interface TenantIdFunction {

    /**
     * 获取租户id
     *
     * @return {@link String}
     */
    String getTenantId();
}
