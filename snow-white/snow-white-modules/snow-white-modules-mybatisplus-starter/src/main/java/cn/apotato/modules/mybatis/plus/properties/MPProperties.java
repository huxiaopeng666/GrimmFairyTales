package cn.apotato.modules.mybatis.plus.properties;


import cn.apotato.modules.mybatis.plus.model.Pagination;
import cn.apotato.modules.mybatis.plus.model.TenantInfo;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 * @author 胡晓鹏
 * @date 2023/05/19
 */
@SuppressWarnings("ALL")
@ConfigurationProperties(prefix = "mybatis-plus")
public class MPProperties {
    private TenantInfo tenant;
    private Pagination pagination;

    public MPProperties() {
        this.tenant = tenant == null ? new TenantInfo() : tenant;
        this.pagination = pagination == null ? new Pagination() : pagination;
    }

    public MPProperties(TenantInfo tenant, Pagination pagination) {
        this.tenant = tenant == null ? new TenantInfo() : tenant;
        this.pagination = pagination == null ? new Pagination() : pagination;
    }

    public TenantInfo getTenant() {
        return tenant;
    }

    public void setTenant(TenantInfo tenant) {
        this.tenant = tenant == null ? new TenantInfo() : tenant;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination == null ? new Pagination() : pagination;
    }
}
