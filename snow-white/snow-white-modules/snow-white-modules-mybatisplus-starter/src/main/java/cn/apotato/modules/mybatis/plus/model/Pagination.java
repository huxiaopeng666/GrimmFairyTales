package cn.apotato.modules.mybatis.plus.model;

/**
 * 分页
 *
 * @author 胡晓鹏
 * @date 2023/05/19
 */
public class Pagination {
    /**
     * 是否启用分页插件，默认: true
     */
    private boolean enable = true;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
