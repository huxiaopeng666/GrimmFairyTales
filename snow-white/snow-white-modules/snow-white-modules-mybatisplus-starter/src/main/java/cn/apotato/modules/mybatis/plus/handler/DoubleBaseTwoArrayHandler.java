package cn.apotato.modules.mybatis.plus.handler;

/**
 * 二维数组 Double类型处理
 * 处理java 对象和数据库之间的转换问题
 * @author 胡晓鹏
 * @date 2023/04/21
 */
public class DoubleBaseTwoArrayHandler extends BaseTwoArrayHandler<Double> {
    /**
     * 得到类型
     *
     * @return {@link Class}<{@link Double}>
     */
    @Override
    protected Class<Double> getType() {
        return Double.class;
    }
}
