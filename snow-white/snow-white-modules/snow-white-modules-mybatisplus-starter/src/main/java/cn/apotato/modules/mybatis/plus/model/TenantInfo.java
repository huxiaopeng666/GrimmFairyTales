package cn.apotato.modules.mybatis.plus.model;

import cn.apotato.modules.mybatis.plus.functions.TenantIdFunction;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 租户信息
 *
 * @author 胡晓鹏
 * @date 2023/05/19
 */
@Data
@NoArgsConstructor
public class TenantInfo {
    /**
     * 是否启用多租户插件，默认: false
     */
    private boolean enable = false;

    /**
     * 租户id
     */
    private String tenantId;
    /**
     * 租户id列
     */
    private String tenantIdColumn;

    /**
     * 忽略表: 不需要拼多租户条件的表
     */
    private List<String> ignoreTable;

    /**
     * 获取租户id的函数
     */
    private TenantIdFunction tenantIdFunction;

    public TenantInfo(boolean enable, String tenantId, List<String> ignoreTable, TenantIdFunction tenantIdFunction) {
        this.enable = enable;
        this.tenantId = tenantId;
        this.ignoreTable = ignoreTable;
        this.tenantIdFunction = tenantIdFunction;
        if (this.tenantIdFunction == null && this.tenantId != null) {
            this.tenantIdFunction = () -> tenantId;
        }
    }

    public void setTenantIdFunction(TenantIdFunction tenantIdFunction) {
        this.tenantIdFunction = tenantIdFunction;
    }

    public String getTenantId() {
        return tenantIdFunction.getTenantId();
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
        if (this.tenantIdFunction == null) {
            this.tenantIdFunction = () -> tenantId;
        }
    }

}
